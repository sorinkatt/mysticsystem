/**
 * @file
 */
(function ($, Drupal) {
  function calcVirtueSpend() {
    let $virtue = 0;
    let $vice = 0;
    let $net = 0;
    $('.virtue-vice-js').each(function() {
      if($(this).is(':checked')) {
        if($(this).is(':checkbox')) {
          if($(this).data('type') === 'virtue') {
            $virtue += parseInt($(this).data('value'))
          } else {
            $vice += parseInt($(this).data('value'))
          }
        } else {
          if($(this).data('type') === 'virtue') {
            $virtue += parseInt($(this).val())
          } else {
            $vice += parseInt($(this).val())
          }
        }
      }
    });
    $net = $vice - $virtue;
    if($net > parseInt(drupalSettings.characters_behaviors.maxfromvirtue)) {
      $net = parseInt(drupalSettings.characters_behaviors.maxfromvirtue);
    }
    $('.total-from-virtues-js').html($net.toString());
  }

  // Function to Calculate Attribute Spend
  function calcSpend() {
    let totalvalue = 0;
    let maxtotal = parseInt($('#attribute-total').text());
    console.log('Skill Points');
    $('.attribute-block-js').each(function () {
      let final = 0;
      let start = $(this).find('.attribute-start');
      let total = $(this).find('.attribute-total');
      let spend = $(this).find('.stat');

      final = parseInt(start.text()) + parseInt(spend.val());
      total.html(final.toString());
      totalvalue += parseInt(spend.val());
    });
    if(totalvalue > maxtotal) {
      $('#attribute-total').addClass('character-alert');
      $('#attribute-spent').addClass('character-alert');
    } else {
      $('#attribute-total').removeClass('character-alert');
      $('#attribute-spent').removeClass('character-alert');
    }
    $('#attribute-spent').html(totalvalue.toString());
  }

  // Function to Calculate Practice Point Spend.
  function calcPracticeSpend() {
      let totalskillvalue = 0;
      let totalaptvalue = 0;
      let maxpracticetotal = parseInt($('#practice-points-total').text());
      let maxaptitudetotal = parseInt($('#aptitude-practice-points-total').text());
      console.log('Practice Points');
    $('.skill-block-js').each(function () {
      let spend = $(this).find('.skill');
      let total = $(this).find('.skill-total');
      let type = spend.data('type');
      let free = parseInt(spend.data('free'));
      if(type == 'aptitude' && ((totalaptvalue + parseInt(spend.val())) <= maxaptitudetotal)) {
        totalaptvalue += parseInt(spend.val()) - free;
      } else {
        totalskillvalue += parseInt(spend.val()) - free;
      }
      total.html( Math.floor(parseInt(spend.val()) / 5) );
    });

    if(totalskillvalue > maxpracticetotal) {
      $('#practice-points-total').addClass('character-alert');
      $('#practice-points-spent').addClass('character-alert');
    } else {
      $('#practice-points-total').removeClass('character-alert');
      $('#practice-points-spent').removeClass('character-alert');
    }
    $('#practice-points-spent').html(totalskillvalue.toString());
    $('#aptitude-practice-points-spent').html(totalaptvalue.toString());
  }

  function checkTabs(deu) {
    console.log(drupalSettings.characters_behaviors);
    if (drupalSettings.characters_behaviors.core === false) {
      $("#tabs").tabs("disable", "#tabs-core");
    } else {
      $("#tabs").tabs("enable", "#tabs-core");
    }
    if (drupalSettings.characters_behaviors.virtues === false) {
      $("#tabs").tabs("disable", "#tabs-virtues");
    } else {
      $("#tabs").tabs("enable", "#tabs-virtues");
    }
    if (drupalSettings.characters_behaviors.skills === false) {
      $("#tabs").tabs("disable", "#tabs-skills");
    } else {
      $("#tabs").tabs("enable", "#tabs-skills");
    }
    if (drupalSettings.characters_behaviors.skillranks === false) {
      $("#tabs").tabs("disable", "#tabs-skill-ranks");
    } else {
      $("#tabs").tabs("enable", "#tabs-skill-ranks");
    }
    if (drupalSettings.characters_behaviors.bloodline === false) {
      $("#tabs").tabs("disable", "#tabs-bloodline");
    } else {
      $("#tabs").tabs("enable", "#tabs-bloodline");
    }
    if (drupalSettings.characters_behaviors.techniques === false) {
      $("#tabs").tabs("disable", "#tabs-techniques");
    } else {
      $("#tabs").tabs("enable", "#tabs-techniques");
    }
    if (drupalSettings.characters_behaviors.powers === false) {
      $("#tabs").tabs("disable", "#tabs-powers");
    } else {
      $("#tabs").tabs("enable", "#tabs-powers");
    }
    if (drupalSettings.characters_behaviors.aptitudes === false) {
      $("#tabs").tabs("disable", "#tabs-aptitudes");
    } else {
      $("#tabs").tabs("enable", "#tabs-aptitudes");
    }
    if (drupalSettings.characters_behaviors.equipment === false) {
      $("#tabs").tabs("disable", "#tabs-equipment");
    } else {
      $("#tabs").tabs("enable", "#tabs-equipment");
    }
  }

  Drupal.behaviors.newcharactersBehavior = {
    attach: function (context, settings) {
      $("#tabs").tabs();
      $('.numeric').each(function () {
        $(this).spinner({
          max: $(this).data('max'),
          min: $(this).data('min'),
          change: function (event, ui) {
            calcSpend();
            calcPracticeSpend();
          }
        }).on('blur', function () {
          if ($(this).data('onInputPrevented')) return;
          var val = this.value,
            $this = $(this),
            max = $this.spinner('option', 'max'),
            min = $this.spinner('option', 'min');
          // We want only number, no alpha.
          // We set it to previous default value.
          if (!val.match(/^[+-]?[\d]{0,}$/)) val = $(this).data('defaultValue');
          this.value = val > max ? max : val < min ? min : val;
        });
      });
      $('#attribute-total').html(drupalSettings.characters_behaviors.attriutepoints);
      $('#practice-points-total').html(drupalSettings.characters_behaviors.practicepoints);
      $('#aptitude-practice-points-total').html(drupalSettings.characters_behaviors.aptitudepoints);
      //We check if each tab is ready and we freeze it.
      calcSpend();
      calcVirtueSpend();
      checkTabs(settings);
      calcPracticeSpend();

      // Active Triggers
      $('.virtue-vice-js', context).unbind().change(function (e) {
        e.stopImmediatePropagation();
        calcVirtueSpend();
      })
    }
  }
})(jQuery, Drupal);
