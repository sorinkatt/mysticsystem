<?php

namespace Drupal\characters\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller to manage character interactions.
 *
 * @ingroup characters
 */
class CharactersController extends ControllerBase
{
  protected $character;

  public function __construct()
  {
    $this->character = \Drupal::service('characters.default');
  }

  public function setCharacter($cid)
  {
    $this->character->setCID($cid);
    $url = Url::fromRoute('characters.new_character')->setAbsolute()->toString();
    #die($url);
    return new RedirectResponse($url);
  }

  public function newCharacter()
  {
    $config = \Drupal::service('config.factory')->getEditable('characters.settings');

    $character = $this->character->loadCharacter($this->character->getCID());
    dpm($character);

    $settings = [
      'virtues' => true,
      'core' => true,
      'skills' => false,
      'skillranks' => false,
      'bloodline' => false,
      'techniques' => false,
      'powers' => false,
      'aptitudes' => false,
      'equipment' => false,
      'attributestart' => $config->get('character_starting_attribute'),
      'attriutepoints' => $character['attributepoints'],
      'maxfromvirtue' => $config->get('character_max_from_virtue'),
      'practicepoints' => $config->get('character_starting_practice_points'),
      'aptitudepoints' => $config->get('character_starting_practice_points_aptitudes'),
    ];

    foreach($character['virtues'] as $virtue) {
      if($virtue['title'] == 'Bloodline') {
        $settings['bloodline'] = true;
      }
    }
    if($character['attributepoints'] == $character['statblock']['totals']['spent']) {
      $settings['skills'] = true;
      $settings['techniques'] = true;
      $settings['powers'] = true;
    }

    if(!empty($character['skills']) && count($character['skills']) > 0) {
      $settings['skillranks'] = true;
    }


    $section['virtues'] = \Drupal::formBuilder()->getForm('\Drupal\characters\Form\VirtuesForm', $character);
    $section['corestats'] = \Drupal::formBuilder()->getForm('\Drupal\characters\Form\CoreStatsForm', $character);
    $section['skills'] = \Drupal::formBuilder()->getForm('\Drupal\characters\Form\SkillsForm', $character);
    $section['skillsranking'] = \Drupal::formBuilder()->getForm('\Drupal\characters\Form\SkillsRankForm', $character);
    $section['powers'] = \Drupal::formBuilder()->getForm('\Drupal\characters\Form\PowerForm', $character);
    $section['aptitudes'] = \Drupal::formBuilder()->getForm('\Drupal\characters\Form\AptitudesForm', $character);
    $section['techniques'] = \Drupal::formBuilder()->getForm('\Drupal\characters\Form\TechniquesForm', $character);
    $section['bloodlines'] = \Drupal::formBuilder()->getForm('\Drupal\characters\Form\BloodlinesForm', $character);

    return [
      '#theme' => 'characters_creation',
      '#sections' => $section,
      '#attached' => [
        'library' => ['characters/characters-primary'],
        'drupalSettings' => ['characters_behaviors' => $settings],
      ],
    ];
  }
}
