<?php

namespace Drupal\characters\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Character List' Block.
 *
 * @Block(
 *   id = "character_block",
 *   admin_label = @Translation("Display Characters for User"),
 *   category = @Translation("Mystic"),
 * )
 */
class CharacterList extends BlockBase
{
  protected $character;

  public function __construct(array $configuration, $plugin_id, $plugin_definition)
  {
    $this->character = \Drupal::service('characters.default');
  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $build = [
      'character_list' => [
        '#theme' => 'characters_list',
        '#lines' => $this->character->listCharacters(),
      ],
    ];

    return $build;
  }
  public function getCacheMaxAge() {
    return 0;
  }

}
