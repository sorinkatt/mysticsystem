<?php


namespace Drupal\characters\Form;


use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\SettingsCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class CoreStatsForm extends FormBase
{

  protected $character;

  /**
   *
   */
  public function __construct()
  {
    $this->character = \Drupal::service('characters.default');
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return 'core_stats_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state, $character = NULL)
  {
    $config = \Drupal::service('config.factory')->getEditable('characters.settings');
    $stats = $character['statblock'];
    //dpm($stats);
    $form = [
      'base_stats' => [
        '#type' => 'container',
        '#attributes' => [
          'id' => 'base-states-container'
        ],
        'form' => [
          'character_name' => [
            '#type' => 'textfield',
            '#title' => 'Character Name',
            '#default_value' => $character['name'],
            '#required' => TRUE,
          ],
          'character_race' => [
            '#type' => 'textfield',
            '#title' => 'Character Race',
            '#default_value' => $character['race'],
          ],
          'character_height' => [
            '#type' => 'textfield',
            '#title' => 'Character Height',
            '#default_value' => $character['height'],
          ],
          'character_weight' => [
            '#type' => 'textfield',
            '#title' => 'Character Weight',
            '#default_value' => $character['weight'],
          ],
          'character_age' => [
            '#type' => 'textfield',
            '#title' => 'Character Age',
            '#default_value' => $character['age'],
          ],
          'character_gender' => [
            '#type' => 'textfield',
            '#title' => 'Character Gender',
            '#default_value' => $character['gender'],
          ],
          'character_focus' => [
            '#title' => 'Focus',
            '#type' => 'select',
            '#required' => TRUE,
            '#default_value' => $character['focus'],
            '#options' => [
              'mana' => 'Mana',
              'stamina' => 'Stamina'
            ]
          ]
        ],
      ],
      'base_attributes' => [
        '#type' => 'container',
        '#attributes' => [
          'id' => 'base-attributes-container'
        ],
        'form' => [
          'markup_top' => [
            '#markup' => '<div class="attribute-block"><span class="edit-coordination attribute-title">&nbsp;</span><span class="attribute-start">Base</span><span class="spinner-block">Spent</span><span class="attribute-total title-span">Total</span></div>'
          ],
          'coordination' => [
            '#prefix' => '<div class="container-inline attribute-block attribute-block-js">
            <span class="edit-coordination attribute-title">Coordination</span>
            <span class="attribute-start">'.$config->get('character_starting_attribute').'</span>',
            '#type' => 'textfield',
            //'#title' => 'Coordination',
            '#default_value' => $stats['co']['start'],
            //'#value' => $config->get('character_starting_attribute'),
            '#attributes' => [
              'class' => ['numeric', 'stat'],
              'data-stat' => 'coordination',
              'data-min' => 0,
              'data-max' => 25
            ],
            '#suffix' => '<span class="attribute-total">'.$stats['co']['total'].'</span></div>',
          ],
          'speed' => [
            '#prefix' => '<div class="container-inline attribute-block attribute-block-js">
            <span class="edit-speed attribute-title">Speed</span>
            <span class="attribute-start">'.$config->get('character_starting_attribute').'</span>',
            '#type' => 'textfield',
            //'#title' => 'Speed',
            '#default_value' => $stats['sp']['start'],
            '#attributes' => [
              'class' => ['numeric', 'stat'],
              'data-stat' => 'speed',
              'data-min' => 0,
              'data-max' => 25
            ],
            '#suffix' => '<span class="attribute-total">'.$stats['sp']['total'].'</span></div>',
          ],
          'hardiness' => [
            '#prefix' => '<div class="container-inline attribute-block attribute-block-js">
            <span class="edit-hardiness attribute-title">Hardiness</span>
            <span class="attribute-start">'.$config->get('character_starting_attribute').'</span>',
            '#type' => 'textfield',
            //'#title' => 'Hardiness',
            '#default_value' => $stats['ha']['start'],
            '#attributes' => [
              'class' => ['numeric', 'stat'],
              'data-stat' => 'hardiness',
              'data-min' => 0,
              'data-max' => 25
            ],
            '#suffix' => '<span class="attribute-total">'.$stats['ha']['total'].'</span></div>',
          ],
          'brawn' => [
            '#prefix' => '<div class="container-inline attribute-block attribute-block-js">
            <span class="edit-brawn attribute-title">Brawn</span>
            <span class="attribute-start">'.$config->get('character_starting_attribute').'</span>',
            '#type' => 'textfield',
            //'#title' => 'Brawn',
            '#default_value' => $stats['br']['start'],
            '#attributes' => [
              'class' => ['numeric', 'stat'],
              'data-stat' => 'brawn',
              'data-min' => 0,
              'data-max' => 25
            ],
            '#suffix' => '<span class="attribute-total">'.$stats['br']['total'].'</span></div>',
          ],
          'presence' => [
            '#prefix' => '<div class="container-inline attribute-block attribute-block-js">
            <span class="edit-presence attribute-title">Presence</span>
            <span class="attribute-start">'.$config->get('character_starting_attribute').'</span>',
            '#type' => 'textfield',
            //'#title' => 'Presence',
            '#default_value' => $stats['pr']['start'],
            '#attributes' => [
              'class' => ['numeric', 'stat'],
              'data-stat' => 'presence',
              'data-min' => 0,
              'data-max' => 25
            ],
            '#suffix' => '<span class="attribute-total">'.$stats['pr']['total'].'</span></div>',
          ],
          'awareness' => [
            '#prefix' => '<div class="container-inline attribute-block attribute-block-js">
            <span class="edit-awareness attribute-title">Awareness</span>
            <span class="attribute-start">'.$config->get('character_starting_attribute').'</span>',
            '#type' => 'textfield',
            //'#title' => 'Awareness',
            '#default_value' => $stats['aw']['start'],
            '#attributes' => [
              'class' => ['numeric', 'stat'],
              'data-stat' => 'awareness',
              'data-min' => 0,
              'data-max' => 25
            ],
            '#suffix' => '<span class="attribute-total">'.$stats['aw']['total'].'</span></div>',
          ],
          'willpower' => [
            '#prefix' => '<div class="container-inline attribute-block attribute-block-js">
            <span class="edit-willpower attribute-title">Willpower</span>
            <span class="attribute-start">'.$config->get('character_starting_attribute').'</span>',
            '#type' => 'textfield',
            //'#title' => 'Willpower',
            '#default_value' => $stats['wp']['start'],
            '#attributes' => [
              'class' => ['numeric', 'stat'],
              'data-stat' => 'willpower',
              'data-min' => 0,
              'data-max' => 25
            ],
            '#suffix' => '<span class="attribute-total">'.$stats['wp']['total'].'</span></div>',
          ],
          'intellect' => [
            '#prefix' => '<div class="container-inline attribute-block attribute-block-js">
            <span class="edit-intellect attribute-title">Intellect</span>
            <span class="attribute-start">'.$config->get('character_starting_attribute').'</span>',
            '#type' => 'textfield',
            //'#title' => 'Intellect',
            '#default_value' => $stats['in']['start'],
            '#attributes' => [
              'class' => ['numeric', 'stat'],
              'data-stat' => 'intellect',
              'data-min' => 0,
              'data-max' => 25
            ],
            '#suffix' => '<span class="attribute-total">'.$stats['in']['total'].'</span></div>',
          ],
        ],
      ],
      'control_buttons' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => 'buttons-control',
        ],
        'buttons' => [
          'save' => [
            '#type' => 'button',
            '#value' => 'Save',
            '#ajax' => [
              'callback' => '::ajaxSave',
              'progress' => [
                'type' => 'throbber',
                'message' => 'Saving Core...',
              ],
            ],
          ]
        ],
        'message' => [
          '#type' => 'markup',
          '#markup' => '<div id="result-message"></div>'
        ],
      ]
    ];
    $form_state->setCached(FALSE);
    $form_state->setRebuild(TRUE);
    return $form;
  }

  public function ajaxSave(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();

    $data = [
      'character' => [
        'name' => $values['character_name'],
        'height' => $values['character_height'],
        'age' => $values['character_age'],
        'weight' => $values['character_weight'],
        'gender' => $values['character_gender'],
        'race' => $values['character_race'],
        'focus' => $values['character_focus'],
      ],
      'core' => [
        'co' => $values['coordination'],
        'sp' => $values['speed'],
        'ha' => $values['hardiness'],
        'br' => $values['brawn'],
        'pr' => $values['presence'],
        'aw' => $values['awareness'],
        'wp' => $values['willpower'],
        'in' => $values['intellect'],
      ]
    ];
    $this->character->saveCore($data);

    $response = new AjaxResponse();

    $settings = [
      'skills' => true,
      'techniques' => true,
      'powers' => true,
      'equipment' => false,
    ];
    $response->addCommand(new SettingsCommand(['characters_behaviors' => $settings], true));
    $response->addCommand(new ReplaceCommand('#core-stats-form', $form));

    return $response;
  }

  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    foreach ($values as $key => $value) {
      switch ($key) {
        case 'character_name':
          if (empty($value)) {
            $message[] = 'Name is Required to save the Character.';
            $valid = false;
          }
      }
    }
    parent::validateForm($form, $form_state); // TODO: Change the autogenerated stub
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();

    $data = [
      'character' => [
        'name' => $values['character_name'],
        'height' => $values['character_height'],
        'age' => $values['character_age'],
        'weight' => $values['character_weight'],
        'gender' => $values['character_gender'],
        'race' => $values['character_race'],
        'focus' => $values['character_focus'],
      ],
      'core' => [
        'co' => $values['coordinartion'],
        'sp' => $values['speed'],
        'ha' => $values['hardiness'],
        'br' => $values['brawn'],
        'pr' => $values['presence'],
        'aw' => $values['awareness'],
        'wp' => $values['willpower'],
        'in' => $values['intellect'],
      ]
    ];
    $this->character->saveCore($data);
  }
}
