<?php


namespace Drupal\characters\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class BloodlinesForm extends FormBase {

  protected $data;

  protected $character;

  /**
   *
   */
  public function __construct() {
    $this->data = \Drupal::service('datamanager.default');
    $this->character = \Drupal::service('characters.default');
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'bloodlines_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state, $character = NULL) {
    $bloodlines = $this->data->getMysticBloodlines();
    $bloodline_list['Major'][0] = '<span class="bloodline-option"><em>None</em>: No Major Bloodline</span>';
    $bloodline_list['Minor'][0] = '<span class="bloodline-option"><em>None</em>: No Minor Bloodline</span>';
    foreach ($bloodlines as $bloodline) {
      $bloodline_list[$bloodline['type']][$bloodline['bid']] = '<span class="bloodline-option"><em>' . $bloodline['title'] . '</em>: ' . $bloodline['description'] . '</span>';
    }

    if(!empty($character['bloodline'])) {
      $form['bloodlines']['current'] = [
        '#type' => 'markup',
        '#markup' => '<div class="bloodline-chosen"><span class="caption">' . $character['bloodline']['title'] . ' (' . $character['bloodline']['type'] . ')</span><p>'.$character['bloodline']['description'] .'</p></div>',
      ];
    }

    $key = 0;
    if (key_exists($character['bloodline']['bid'], $bloodline_list['Minor'])) {
      $key = $character['bloodline']['bid'];
    }
    $form['bloodlines']['minor'] = [
      '#type' => 'details',
      '#title' => 'Minor Bloodlines (' . (count($bloodline_list['Minor']) - 1) . ')',
      '#collapsable' => TRUE,
      'minorlist' => [
        '#type' => 'radios',
        '#options' => $bloodline_list['Minor'],
        '#default_value' => $key,
      ],
      '#attributes' => [
        'class' => ['bloodline-select'],
      ],
      '#suffix' => '<br/>',
    ];
    $key = 0;
    if (key_exists($character['bloodline']['bid'], $bloodline_list['Major'])) {
      $key = $character['bloodline']['bid'];
    }
    $form['bloodlines']['major'] = [
      '#type' => 'details',
      '#title' => 'Major Bloodlines  (' . (count($bloodline_list['Major']) - 1) . ')',
      '#collapsable' => TRUE,
      '#collapsed' => FALSE,
      'majorlist' => [
        '#type' => 'radios',
        '#options' => $bloodline_list['Major'],
        '#default_value' => $key,
      ],
      '#attributes' => [
        'class' => ['bloodline-select'],
      ],
      '#suffix' => '<br/>',
    ];
    $form['save_two'] = [
      '#type' => 'submit',
      '#value' => 'Save',
    ];
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    if ($values['minorlist'] > 0 && $values['majorlist'] > 0) {
      $form_state->setErrorByName('minorlist', t('You can not have both a Minor and Major Bloodline.'));
    }
    // If validation errors, save them to the hidden form field in JSON format
    if ($errors = $form_state->getErrors()) {
      $form['my_module_error_msgs']['#value'] = json_encode($errors);
    }

    return;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if ($values['minorlist'] == 0 && $values['majorlist'] == 0) {
      $this->character->removeBloodline();
    }
    elseif ($values['minorlist'] > 0) {
      $this->character->removeBloodline();
      $this->character->saveBloodline($values['minorlist']);
    }
    elseif ($values['majorlist'] > 0) {
      $this->character->removeBloodline();
      $this->character->saveBloodline($values['majorlist']);
    }

  }

}
