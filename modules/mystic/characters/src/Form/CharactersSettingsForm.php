<?php

namespace Drupal\characters\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class CharactersSettingsForm extends ConfigFormBase
{

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'characters.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'characters_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config(static::SETTINGS);
    $form['starting_attribute'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Starting Attribute value'),
      '#default_value' => $config->get('character_starting_attribute'),
    ];
    $form['starting_attributes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Starting Attribute points'),
      '#default_value' => $config->get('character_starting_attributes'),
    ];
    $form['max_vices'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum Points From Vices'),
      '#default_value' => $config->get('character_max_vices'),
    ];
    $form['starting_base_life'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Starting Life'),
      '#default_value' => $config->get('character_starting_life'),
    ];
    $form['starting_practice_points'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Starting Practice Points'),
      '#default_value' => $config->get('character_starting_practice_points'),
    ];
    $form['starting_practice_points_aptitudes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Starting Practice Points for Aptitudes'),
      '#default_value' => $config->get('character_starting_practice_points_aptitudes'),
    ];
    $form['character_max_aptitudes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum Practice Points for Aptitudes and Skills'),
      '#default_value' => $config->get('character_max_aptitudes'),
    ];
    $form['character_max_from_virtue'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum Points from Vices'),
      '#default_value' => $config->get('character_max_from_virtue'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
      $this->configFactory->getEditable(static::SETTINGS)
      ->set('character_starting_attribute', $form_state->getValue('starting_attribute'))
      ->set('character_starting_attributes', $form_state->getValue('starting_attributes'))
      ->set('character_max_vices', $form_state->getValue('max_vices'))
      ->set('character_starting_life', $form_state->getValue('starting_base_life'))
      ->set('character_starting_practice_points', $form_state->getValue('starting_practice_points'))
      ->set('character_starting_practice_points_aptitudes', $form_state->getValue('starting_practice_points_aptitudes'))
      ->set('character_max_from_virtue', $form_state->getValue('character_max_from_virtue'))
      ->set('character_max_aptitudes', $form_state->getValue('character_max_aptitudes'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
