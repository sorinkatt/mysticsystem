<?php


namespace Drupal\characters\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class SkillsForm extends FormBase {

  protected $data;

  protected $character;

  /**
   *
   */
  public function __construct() {
    $this->data = \Drupal::service('datamanager.default');
    $this->character = \Drupal::service('characters.default');
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'skill_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state, $character = NULL) {
    $form = [];
    $skilldata = $this->data->getMysticSkills();
    $skilllist = [];
    $skilldefault = [];
    foreach ($skilldata as $skill) {
      if ($skill['default'] > 0) {
        $skilldefault[$skill['type']][$skill['sid']] = $skill['sid'];
      }
      $skilllist[$skill['type']][$skill['sid']] = $skill['title'];
    }
    foreach ($character['skills'] as $skill) {
      $skilldefault[$skill['type']][$skill['sid']] = $skill['sid'];
    }

    $form['startpicks'] = [
      '#type' => 'container',
    ];

    $skilltemplist = [];
    if (1) {
      $templist = $this->data->getMysticSkillsbyType('Weapon Proficiency');

      foreach ($templist as $skill) {
        $skilltemplist[$skill['sid']] = $skill['title'];
      }
      $form['startpicks']['weapon_prof'] = [
        '#type' => 'select',
        '#title' => 'Free Weapon Proficiency',
        '#options' => $skilltemplist,
        '#required' => TRUE
      ];
    }
    else {
      $form['startpicks']['weapon_prof'] = [
        '#type' => 'markup',
        '#markup' => '<div class="chosen-base-skill">Chosen Skill for this Category <span class="clear-skill" data-skill="#skid"> - Clear</span></div>'
      ];
    }

    $skilltemplist = [];
    if (1) {
      $templist = $this->data->getMysticSkillsbyType('Language');
      foreach ($templist as $skill) {
        $skilltemplist[$skill['sid']] = $skill['title'];
      }
      $form['startpicks']['language'] = [
        '#type' => 'select',
        '#title' => 'Language Skill',
        '#options' => $skilltemplist,
        '#required' => TRUE
      ];
    }
    else {
      $form['startpicks']['language'] = [
        '#type' => 'markup',
        '#markup' => '<div class="chosen-base-skill">Chosen Skill for this Category <span class="clear-skill" data-skill="#skid"> - Clear</span></div>'
      ];
    }

    $skilltemplist = [];
    if (1) {
      $templist = $this->data->getMysticSkillsbyType('Persuasion');
      foreach ($templist as $skill) {
        $skilltemplist[$skill['sid']] = $skill['title'];
      }
      $form['startpicks']['persuasion'] = [
        '#type' => 'select',
        '#title' => 'Persuasion Skill',
        '#options' => $skilltemplist,
        '#required' => TRUE
      ];
    }
    else {
      $form['startpicks']['persuasion'] = [
        '#type' => 'markup',
        '#markup' => '<div class="chosen-base-skill">Chosen Skill for this Category <span class="clear-skill" data-skill="#skid"> - Clear</span></div>'
      ];
    }


    $cnt = 0;
    $col = 1;
    foreach ($skilllist as $category => $skills) {
      if ($cnt == 2) {
        $cnt = 0;
        $col++;
      }

      $form['column-' . $col][$category . '_skilllist'] = [
        '#type' => 'checkboxes',
        '#title' => ucfirst($category),
        '#options' => $skills,
        '#default_value' => $skilldefault[$category],
        '#tree' => TRUE,
        '#attributes' => [
          'class' => ['skills-select'],
        ],
      ];
      $cnt++;
    }
    $form['column-1']['#type'] = 'fieldset';
    $form['column-1']['#attributes'] = [
      'class' => ['edit-column'],
    ];
    $form['column-2']['#type'] = 'fieldset';
    $form['column-2']['#attributes'] = [
      'class' => ['edit-column'],
    ];
    $form['column-3']['#type'] = 'fieldset';
    $form['column-3']['#attributes'] = [
      'class' => ['edit-column'],
    ];
    $form['column-4']['#type'] = 'fieldset';
    $form['column-4']['#attributes'] = [
      'class' => ['edit-column'],
    ];
    $form['save'] = [
      '#type' => 'submit',
      '#value' => 'Save',
      '#attributes' => [
        'class' => ['skills-clear'],
      ],
      '#prefix' => "<p>Selecting skills from this list will add them at a rank of 0.  Leaving 0 based skills on the sheet will make it so that you don't have to buy them after character creation and can lead to miss calculations for training.</p>",
    ];
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state); // TODO: Change the autogenerated stub
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $skills = [];
    $values = $form_state->getValues();
    foreach ($values['combat_skilllist'] as $skill) {
      if ($skill != 0) {
        $skills[] = $skill;
      }
    }
    foreach ($values['knowledge_skilllist'] as $skill) {
      if ($skill != 0) {
        $skills[] = $skill;
      }
    }
    foreach ($values['domestic_skilllist'] as $skill) {
      if ($skill != 0) {
        $skills[] = $skill;
      }
    }
    foreach ($values['persuasion_skilllist'] as $skill) {
      if ($skill != 0) {
        $skills[] = $skill;
      }
    }
    foreach ($values['field_skilllist'] as $skill) {
      if ($skill != 0) {
        $skills[] = $skill;
      }
    }
    foreach ($values['trade_skilllist'] as $skill) {
      if ($skill != 0) {
        $skills[] = $skill;
      }
    }
    foreach ($values['espionage_skilllist'] as $skill) {
      if ($skill != 0) {
        $skills[] = $skill;
      }
    }
    foreach ($values['majik_skilllist'] as $skill) {
      if ($skill != 0) {
        $skills[] = $skill;
      }
    }
    $this->character->addNewSkills($skills);
    dump($skills);
    die();
  }

}
