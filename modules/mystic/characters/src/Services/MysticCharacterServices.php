<?php

namespace Drupal\characters\Services;

use Drupal\Core\Link;
use Drupal\Core\Url;
use http\Exception\InvalidArgumentException;
use PDO;

/**
 * Class MysticCharacterServices.
 *
 * @package Drupal\characters\Services
 */
class MysticCharacterServices {

  protected $connection;

  protected $config;

  protected $store;

  protected $character;


  public function __construct() {
    $this->connection = \Drupal::database();
    $this->store = \Drupal::service('user.private_tempstore')
      ->get('characters');
    $this->config = \Drupal::service('config.factory')
      ->getEditable('characters.settings');
    $this->character = NULL;
  }

  public function setCID($cid) {
    $this->store->set('character_id', $cid);
  }

  public function getCID($create = FALSE) {
    if (!empty($this->store->get('character_id'))) {
      return $this->store->get('character_id');
    }
    else {
      if ($create) {
        $uid = \Drupal::currentUser()->id();
        $id = $this->connection->insert('characters')
          ->fields([
            'player' => $uid,
            'created' => time(),
          ])
          ->execute();
        $this->store->set('character_id', $id);
        return $this->store->get('character_id');
      }
      else {
        return NULL;
      }
    }
  }

  public function listCharacters($format = 'list', $admin = 0) {
    $return = [];
    $uid = \Drupal::currentUser()->id();
    $query = $this->connection->select('characters', 'cha')
      ->condition('player', $uid)
      ->fields('cha')->execute();

    while ($row = $query->fetchAssoc()) {
      #dpm($row);
      switch ($format) {
        case 'list':
          $url = '';
          if ($row['approved'] == 1) {
            $return[] = vsprintf('<li class="list-element">%s</li>', [
              $row['name'],
            ]);

          }
          else {
            $url = Link::fromTextAndUrl($row['name'], Url::fromRoute('characters.toggle_edit', ['cid' => $row['cid']]))
              ->toString();
            $return[] = vsprintf('<li class="list-element">%s</li>', [
              $url,
            ]);
          }

          break;
      }
    }

    return $return;
  }

  public function loadCharacter($cid) {
    $query = $this->connection->select('characters', 'cha')
      ->condition('cid', (int) $cid)
      ->fields('cha')->execute();
    $character = $query->fetchAssoc();

    $character['statblock'] = $this->getStatBlocks();
    $character['virtues'] = $this->loadVirtues($cid, 'virtue');
    $character['vices'] = $this->loadVirtues($cid, 'vice');
    $character['bloodline'] = $this->loadBloodline($cid);
    $character['skills'] = $this->loadSkills($cid);

    $character['attributepoints'] = $this->calculateAttributePoints($character);

    $this->character = $character;
    return $character;
  }

  public function saveVirtue($data) {
    $count = $this->connection->select('character_virtues_vices', 'cvv')
      ->condition('cid', $data['cid'])
      ->condition('vid', $data['vid'])
      ->fields('cvv', ['cvid'])
      ->execute()->fetchAssoc();
    if (empty($count)) {
      $this->connection->insert('character_virtues_vices')
        ->fields($data)
        ->execute();
    }
    else {
      $this->connection->update('character_virtues_vices')
        ->condition('cvid', $count['cvid'])
        ->fields($data)
        ->execute();
    }
  }

  public function deleteVirtue($vid, $cid) {
    $this->connection->delete('character_virtues_vices')
      ->condition('cid', $cid)
      ->condition('vid', $vid)
      ->execute();
  }

  public function saveCore($data) {
    $cid = $this->getCID();

    // Save Character Core
    $this->connection->update('characters')
      ->fields($data['character'])
      ->condition('cid', $cid)
      ->execute();

    // Save each attribute
    foreach ($data['core'] as $key => $core) {
      $this->connection->merge('character_attributes')
        ->keys(['cid', 'attribute'], ['cid' => $cid, 'attribute' => $key])
        ->insertFields([
          'cid' => $cid,
          'attribute' => $key,
          'starting_value' => $this->config->get('character_starting_attribute'),
          'level_spend' => 0,
          'bonus' => 0,
          'start' => (int) $core,
          'notes' => '',
          'created' => time(),
        ])->updateFields([
          'starting_value' => $this->config->get('character_starting_attribute'),
          'level_spend' => 0,
          'bonus' => 0,
          'start' => (int) $core,
          'notes' => '',
        ])->execute();
    }
  }

  public function saveBloodline($id, $pp_spend = 0) {
    $cid = $this->getCID();
    $count = $this->connection->select('character_bloodlines', 'cbl')
      ->condition('cid', $cid)
      ->fields('cbl', ['cbid'])
      ->execute()->fetchAssoc();
    if (empty($count)) {
      $this->connection->insert('character_bloodlines')
        ->fields([
          'bid' => $id,
          'cid' => $cid,
          'pp_spend' => 5,
          'created' => time(),
        ])->execute();
    }
    else {
      if ($pp_spend > 0) {
        $this->connection->update('character_bloodlines')
          ->condition('cid', $cid)
          ->fields([
            'pp_spend' => $pp_spend,
          ])->execute();
      }
      else {
        $this->connection->update('character_bloodlines')
          ->condition('cid', $cid)
          ->fields([
            'bid' => $id,
            'cid' => $cid,
          ])->execute();
      }
    }
  }

  public function removeBloodline() {
    $cid = $this->getCID();
    $this->connection->delete('character_bloodlines')
      ->condition('cid', $cid)
      ->execute();
    $this->connection->delete('character_aptitudes')
      ->condition('cid', $cid)
      ->condition('bloodline', 0, ">")
      ->execute();
  }

  public function addNewSkills($skills) {
    $cid = $this->getCID();
    $currentskills = $this->loadSkills($cid);
    $skillkeys = array_keys($currentskills);
    dump($currentskills);
    foreach ($skills as $skill) {
      if (array_key_exists($skill, $currentskills)) {
        // Skill Exists
        $key = array_search($skill, $skillkeys);
        unset($skillkeys[$key]);
      }
      else {
        // Add the skill
        $this->connection->insert('character_skills')
          ->fields([
            'sid' => $skill,
            'cid' => $cid
          ])->execute();
      }
    }
    dump($skillkeys);
    if(count($skillkeys) > 0) {
      // Delete all skills in remaining list.
      foreach($skillkeys as $sid) {
        $this->connection->delete('character_skills')
          ->condition('cid', $cid)
          ->condition('sid', $sid)
          ->execute();
      }
    }
  }

  public function getStatBlocks() {
    $cid = $this->getCID();
    $attributes = ['co', 'sp', 'ha', 'br', 'pr', 'aw', 'wp', 'in'];
    $spend = 0;
    $level = 0;
    $return = [];
    if ($cid) {
      foreach ($attributes as $att) {
        $return[$att] = [
          'base' => $this->config->get('character_starting_attribute'),
          'start' => 0,
          'spent' => 0,
          'level' => 0,
          'bonus' => 0,
          'total' => $this->config->get('character_starting_attribute'),
        ];
      }
      $return['totals'] = [
        'spent' => 0,
        'level' => 0,
      ];
      $records = $this->connection->select('character_attributes', 'cat')
        ->condition('cid', $cid)
        ->fields('cat')
        ->execute();
      while ($row = $records->fetchAssoc()) {
        $return[$row['attribute']] = [
          'base' => $row['starting_value'],
          'start' => $row['start'],
          'level' => $row['level_spend'],
          'bonus' => $row['bonus'],
          'total' => ($row['starting_value'] + $row['start'] + $row['level_spend'] + $row['bonus']),
        ];
        $spend += $row['start'];
        $level += $row['level_spend'];
      }
      $return['totals'] = [
        'spent' => $spend,
        'level' => $level,
      ];
    }
    else {
      foreach ($attributes as $att) {
        $return[$att] = [
          'base' => $this->config->get('character_starting_attribute'),
          'start' => 0,
          'level' => 0,
          'bonus' => 0,
          'total' => $this->config->get('character_starting_attribute'),
        ];
      }
      $return['totals'] = [
        'spent' => 0,
        'level' => 0,
      ];
    }
    return $return;
  }

  public function loadVirtues($cid, $type) {
    $records = $this->connection->select('character_virtues_vices', 'cvv')
      ->condition('cvv.cid', $cid)
      ->condition('cvv.type', $type);
    $records->join('mysticvirtues', 'miv', "cvv.vid= miv.vid");
    $records->fields('cvv');
    $records->fields('miv');
    return $records->execute()->fetchAllAssoc('vid', PDO::FETCH_ASSOC);
  }

  public function loadBloodline($cid) {
    $query = $this->connection->select('character_bloodlines', 'cbl')
      ->condition('cid', $cid);
    $query->join('mysticbloodline', 'mbl', "mbl.bid=cbl.bid");
    $query->fields('cbl');
    $query->fields('mbl');
    $record = $query->execute()->fetchAssoc();
    $record['level'] = floor($record['pp_spend'] / 5);
    $record['tier1'] = $this->loadBloodlineVirtues($record['bid'], 1); // Get Tier one Virtues
    $record['tier2'] = $this->loadBloodlineVirtues($record['bid'], 2); // Get Tier two Virtues
    $record['tier3'] = $this->loadBloodlineVirtues($record['bid'], 3); // Get Tier three Virtues
    return $record;
  }

  public function loadSkills($cid) {
    $query = $this->connection->select('character_skills', 'csl')
      ->condition('cid', $cid);
    $query->join('mysticskills', 'mss', "mss.sid=csl.sid");
    $query->fields('csl');
    $query->fields('mss');
    return $query->execute()->fetchAllAssoc('sid', PDO::FETCH_ASSOC);
  }

  private function loadBloodlineVirtues($bid, $tier) {
    $return = [];
    $cid = $this->getCID();
    $query = $this->connection->select('mysticaptitudes', 'cbl')
      ->condition('cbl.tier', $tier)
      ->condition('cbl.bloodline', $bid);
    $query->leftJoin('character_aptitudes', 'cap', "cap.capid=cbl.aid AND cap.cid = " . $cid);
    $query->fields('cbl');
    $query->fields('cap');
    $records = $query->execute();
    while ($row = $records->fetchAssoc()) {
      $return[] = $row;
    }
    return $return;
  }

  private function calculateAttributePoints($record) {
    // Get Base Points
    $total = $this->config->get('character_starting_attributes');
    // Calculate Total from Virtues & Vices
    foreach ($record['virtues'] as $vir) {
      $total = $total - $vir['cost'];
    }
    $vicetotal = 0;
    foreach ($record['vices'] as $vir) {
      if (
        $vicetotal < $this->config->get('character_max_from_virtue') &&
        ($vicetotal + $vir['cost']) < $this->config->get('character_max_from_virtue')
      ) {
        $total = $total + $vir['cost'];
        $vicetotal += $vir['cost'];
      }
      elseif (
        $vicetotal < $this->config->get('character_max_from_virtue') &&
        ($vicetotal + $vir['cost']) > $this->config->get('character_max_from_virtue')
      ) {
        $total = $total + $this->config->get('character_max_from_virtue') - $vicetotal;
        $vicetotal = $this->config->get('character_max_from_virtue');
      }
    }
    return $total;
  }

}
