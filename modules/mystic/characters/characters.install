<?php

/**
 * Implements hook_schema().
 *
 * This defines the database table which will hold the example item info.
 *
 * @ingroup tabledrag_example
 */
function characters_schema()
{
  $schema['characters'] = [
    'fields' => [
      'cid' => [
        'description' => 'The primary identifier for each charcter',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'player' => [
        'description' => 'Player ID',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'name' => [
        'description' => 'Character Name',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => 'New Character',
      ],
      'height' => [
        'description' => 'Height',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ],
      'weight' => [
        'description' => 'Character Weight',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ],
      'gender' => [
        'description' => 'Character Gender',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ],
      'age' => [
        'description' => 'Character Age',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ],
      'focus' => [
        'description' => 'Character Focus',
        'type' => 'int',
        'length' => 1,
        'not null' => TRUE,
        'default' => 0,
      ],
      'race' => [
        'description' => 'Character Race',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ],
      'life' => [
        'description' => 'Campaign',
        'type' => 'int',
        'length' => 11,
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'created' => [
        'description' => 'Created Date',
        'type' => 'int',
        'length' => 11,
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'approved' => [
        'description' => 'Admin Approved',
        'type' => 'int',
        'length' => 11,
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'campaign' => [
        'description' => 'Campaign',
        'type' => 'int',
        'length' => 11,
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['cid'],
  ];
  $schema['character_virtues_vices'] = [
    'fields' => [
      'cvid' => [
        'description' => 'The primary identifier for each virtue',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'vid' => [
        'description' => 'Virtue ID',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'cid' => [
        'description' => 'Character ID',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'option' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ],
      'cost' => [
        'description' => 'Cost',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'type' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ],
      'created' => [
        'description' => 'Created Date',
        'type' => 'int',
        'length' => 11,
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['cvid'],
  ];
  $schema['character_attributes'] = [
    'fields' => [
      'caid' => [
        'description' => 'The primary identifier for each attribute entry',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'cid' => [
        'description' => 'Character ID',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'attribute' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ],
      'starting_value' => [
        'description' => 'Starting Attribute',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'level_spend' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'bonus' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'start' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'notes' => [
        'type' => 'varchar',
        'length' => 5000,
        'not null' => FALSE,
        'default' => '',
      ],
      'created' => [
        'description' => 'Created Date',
        'type' => 'int',
        'length' => 11,
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['caid'],
  ];
  $schema['character_bloodlines'] = [
    'fields' => [
      'cbid' => [
        'description' => 'The primary identifier for each attribute entry',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'bid' => [
        'description' => 'Character ID',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'cid' => [
        'description' => 'Character ID',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'pp_spend' => [
        'description' => 'Starting Attribute',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'created' => [
        'description' => 'Created Date',
        'type' => 'int',
        'length' => 11,
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['cbid'],
  ];
  $schema['character_skills'] = [
    'fields' => [
      'csid' => [
        'description' => 'The primary identifier for each skill entry',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'cid' => [
        'description' => 'Character ID',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'sid' => [
        'description' => 'Skill ID',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'variable_title' => [
        'description' => 'Text if the skill has a specification such as language, etc',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ],
      'exelenices' => [
        'type' => 'varchar',
        'length' => 5000,
        'not null' => FALSE,
        'default' => '',
      ],
      'pp_spend' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'pp_bonus' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'pp_free' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'pp_crit' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'class' => [
        'type' => 'int',
        'length' => 1,
        'not null' => TRUE,
        'default' => 0,
      ],
      'notes' => [
        'type' => 'varchar',
        'length' => 5000,
        'not null' => FALSE,
        'default' => '',
      ],
      'created' => [
        'description' => 'Created Date',
        'type' => 'int',
        'length' => 11,
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['csid'],
  ];
  $schema['character_aptitudes'] = [
    'fields' => [
      'capid' => [
        'description' => 'The primary identifier for each aptitude entry',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'cid' => [
        'description' => 'Character ID',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'aid' => [
        'description' => 'Aptitude ID',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'bloodline' => [
        'description' => 'Bloodline the aptitude is linked to',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'variable_title' => [
        'description' => 'Text if the aptitude has a specification such as language, etc',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ],
      'pp_spend' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'pp_bonus' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'pp_free' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'pp_crit' => [
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'class' => [
        'type' => 'int',
        'length' => 1,
        'not null' => TRUE,
        'default' => 0,
      ],
      'notes' => [
        'type' => 'varchar',
        'length' => 5000,
        'not null' => FALSE,
        'default' => '',
      ],
      'created' => [
        'description' => 'Created Date',
        'type' => 'int',
        'length' => 11,
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['capid'],
  ];
  $schema['character_journal'] = [
    'fields' => [
      'jid' => [
        'description' => 'The primary identifier for each journal',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'cid' => [
        'description' => 'Character ID',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'default' => 0,
      ],
      'notes' => [
        'type' => 'varchar',
        'length' => 5000,
        'not null' => FALSE,
        'default' => '',
      ],
      'omegas' => [
        'type' => 'int',
        'length' => 1,
        'not null' => TRUE,
        'default' => 0,
      ],
      'created' => [
        'description' => 'Created Date',
        'type' => 'int',
        'length' => 11,
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['jid'],
  ];
  return $schema;
}

