/**
 * @file
 */
(function ($, Drupal) {

  function recordFilters($filteritems) {
    $.ajax({
      dataType: 'json',
      method: "POST",
      url: "/admin/datamanger/aptitudes/ajax",
      data: {'elements': $filteritems}
    }).done(function (data) {
      if(data) {
        location.reload(true);
      }
    });
  }

  Drupal.behaviors.datamanager = {
    attach(context, settings) {
      $('.aptitude-filter-toggle', context).click(function () {
        $('.aptitude-filter-list').toggleClass('showData');
      });
      $('.data-listing', context).change(function () {
        jsonObj = [];
        $('.data-listing').each(function () {
          if ($(this).is(':checked')) {
            //item = {}
            item = $(this).val();
            jsonObj.push(item);
          }
        });
        recordFilters(jsonObj);
      });
    }
  };
})(jQuery, Drupal);
