<?php

namespace Drupal\datamanager\Services;

/**
 * Class InternationalizationServices.
 *
 * @package Drupal\internationalization\Services
 */
class MysticDataService
{

  public function getMysticSkillfromName($name)
  {
    $skill = $this->getMysticSkills($name);
    if (is_array($skill) && !empty($skill[0])) {
      $skill = $skill[0];
    }
    return $skill;
  }

  public function getMysticSkills($skill = NULL, $start = 0, $perpage = 0)
  {
    $skillarray = [];
    $database = \Drupal::database();
    $query = $database->select('mysticskills', 'msk');
    $query->fields('msk');
    // Conditional Skill
    if (!empty($skill) && is_numeric($skill)) {
      $query->condition('sid', $skill);
    } else if (!empty($skill)) {
      $query->condition('title', $skill);
    }
    // Paging for function
    if ($start > 0 && $perpage > 0) {
      $query->range($start, $perpage);
    }

    $result = $query->execute();
    while ($row = $result->fetchAssoc()) {
      $skillarray[] = $row;
    }

    return $skillarray;
  }

  public function getMysticSkillsbyType($type) {
    $skillarray = [];
    $database = \Drupal::database();
    $result = $database->select('mysticskills', 'msk')
      ->condition('subtype', $type)
      ->fields('msk')
      ->execute();
    while ($row = $result->fetchAssoc()) {
      $skillarray[] = $row;
    }
    return $skillarray;
  }

  public function getMysticAptitudefromName($name)
  {
    $aptitude = $this->getMysticAptitudes($name);
    if (is_array($aptitude) && !empty($aptitude[0])) {
      $aptitude = $aptitude[0];
    }
    return $aptitude;
  }

  public function getMysticAptitudes($aptitude = NULL, $start = 0, $perpage = 0)
  {
    $aptitudearray = [];
    $database = \Drupal::database();
    $query = $database->select('mysticaptitudes', 'mak');
    $query->fields('mak');
    // Conditional Skill
    if (!empty($aptitude) && is_numeric($aptitude)) {
      $query->condition('aid', $aptitude);
    } else if (!empty($aptitude)) {
      $query->condition('title', $aptitude);
    }
    // Paging for function
    if ($start > 0 && $perpage > 0) {
      $query->range($start, $perpage);
    }

    $result = $query->execute();
    while ($row = $result->fetchAssoc()) {
      $aptitudearray[] = $row;
    }

    return $aptitudearray;
  }

  public function getMysticBloodlinefromName($name)
  {
    $bloodline = $this->getMysticBloodlines($name);
    if (is_array($bloodline) && !empty($bloodline[0])) {
      $bloodline = $bloodline[0];
    }
    return $bloodline;
  }

  public function getMysticBloodlines($bloodline = NULL, $start = 0, $perpage = 0) {
    $bloodlinearray = [];
    $database = \Drupal::database();
    $query = $database->select('mysticbloodline', 'mbl');
    $query->fields('mbl');
    // Conditional Skill
    if (!empty($bloodline) && is_numeric($bloodline)) {
      $query->condition('bid', $bloodline);
    } else if (!empty($bloodline)) {
      $query->condition('title', $bloodline);
    }
    // Paging for function
    if ($start > 0 && $perpage > 0) {
      $query->range($start, $perpage);
    }

    $result = $query->execute();
    while ($row = $result->fetchAssoc()) {
      $bloodlinearray[] = $row;
    }

    return $bloodlinearray;
  }

  public function getMysticVirtuefromName($name)
  {
    $virtue = $this->getMysticVirtue($name);
    if (is_array($virtue) && !empty($virtue[0])) {
      $virtue = $virtue[0];
    }
    return $virtue;
  }

  public function getMysticVirtue($virtue = NULL, $start = 0, $perpage = 0) {
    $virtuearray = [];
    $database = \Drupal::database();
    $query = $database->select('mysticvirtues', 'mvs');
    $query->fields('mvs');
    // Conditional Skill
    if (!empty($virtue) && is_numeric($virtue)) {
      $query->condition('vid', $virtue);
    } else if (!empty($virtue)) {
      $query->condition('title', $virtue);
    }
    // Paging for function
    if ($start > 0 && $perpage > 0) {
      $query->range($start, $perpage);
    }

    $result = $query->execute();
    while ($row = $result->fetchAssoc()) {
      $virtuearray[] = $row;
    }

    return $virtuearray;
  }

  public function getMysticVirtueOption($vid, $cost) {
    $database = \Drupal::database();
    $query = $database->select('mysticvirtues', 'mvs')
      ->condition('vid', $vid)
      ->fields('mvs', ['options'])
      ->execute()->fetchAssoc();
    $option = json_decode($query['options']);
    foreach($option as $opt) {
      if($opt->value == $cost) {
        return $opt->option;
      }
    }

    return '';
  }

  public function getMysticLanguagefromName($name)
  {
    $virtue = $this->getMysticLanguage($name);
    if (is_array($virtue) && !empty($virtue[0])) {
      $virtue = $virtue[0];
    }
    return $virtue;
  }

  public function getMysticLanguage($language = NULL, $start = 0, $perpage = 0) {
    $languagearray = [];
    $database = \Drupal::database();
    $query = $database->select('mysticlanguage', 'mls');
    $query->fields('mls');
    // Conditional Skill
    if (!empty($language) && is_numeric($language)) {
      $query->condition('lid', $language);
    } else if (!empty($language)) {
      $query->condition('title', $language);
    }
    // Paging for function
    if ($start > 0 && $perpage > 0) {
      $query->range($start, $perpage);
    }

    $result = $query->execute();
    while ($row = $result->fetchAssoc()) {
      $languagearray[] = $row;
    }

    return $languagearray;
  }
//getMysticCampaignSetting
  public function getMysticCampaignSettingfromName($name)
  {
    $virtue = $this->getMysticCampaignSetting($name);
    if (is_array($virtue) && !empty($virtue[0])) {
      $virtue = $virtue[0];
    }
    return $virtue;
  }

  public function getMysticCampaignSetting($campaign = NULL, $start = 0, $perpage = 0) {
    $campaignarray = [];
    $database = \Drupal::database();
    $query = $database->select('mysticcampaignsetting', 'mcs');
    $query->fields('mcs');
    // Conditional Skill
    if (!empty($campaign) && is_numeric($campaign)) {
      $query->condition('csid', $campaign);
    } else if (!empty($language)) {
      $query->condition('title', $campaign);
    }
    // Paging for function
    if ($start > 0 && $perpage > 0) {
      $query->range($start, $perpage);
    }

    $result = $query->execute();
    while ($row = $result->fetchAssoc()) {
      $campaignarray[] = $row;
    }

    return $campaignarray;
  }

  public function getOwnerOptions() {
    $options = [];
    $database = \Drupal::database();
    $query = $database->select('users', 'usr');
    $query->leftJoin('users_field_data','ufd', 'ufd.uid = usr.uid');
    $query->fields('usr', ['uid']);
    $query->fields('ufd', ['name']);
    $results = $query->execute();
    while($row = $results->fetchAssoc()) {
      if($row['name'] == '') {
        $options[$row['uid']] = 'Unowned';
      } else {
        $options[$row['uid']] = $row['name'];
      }
    }
    ksort($options);
    return $options;
  }
}
