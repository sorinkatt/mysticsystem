<?php

namespace Drupal\datamanager\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DatamanagerAdminController.
 */
class DatamanagerAdminController extends ControllerBase
{
  protected $dataservice;
  protected $database;
  protected $tempstore;

  public function __construct()
  {
    $this->dataservice = \Drupal::service('datamanager.default');
    $this->database = \Drupal::database();
    $this->tempstore = \Drupal::request()->getSession();
  }

  public function datamanagerHome()
  {

    return ['#theme' => 'datamanager_admin'];
  }

  public function datamanagerSkills()
  {
    // Prepare _sortable_ table header
    $header = [
      ['data' => t('ID'), 'field' => 'sid'],
      ['data' => t('Name'), 'field' => 'title'],
      ['data' => t('Type'), 'field' => 'type'],
      ['data' => t('Attributes'), 'field' => 'attribute', 'sort' => 'desc'],
      ['data' => t('Variable'), 'field' => 'variable'],
    ];

    $query = $this->database->select('mysticskills', 'loc');
    $query->fields('loc', ['sid', 'title', 'type', 'attribute', 'variable']);
    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header);
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
    $result = $pager->execute();

    foreach ($result as $row) {
      $row = (array)$row;
      $row['variable'] = (['variable'] == 1 ? 'Yes' : 'No');
      $row['title'] =  Link::createFromRoute($row['title'], 'datamanager.admin.skills.edit', [ 'sid' => $row['sid'] ]);
      $row['attribute'] = t(implode("<br/>", json_decode($row['attribute'])));
      $rows[] = ['data' => $row];
    }

    $build['location_table'] = array(
      '#theme' => 'table', '#header' => $header,
      '#rows' => $rows
    );
    $build['pager'] = [
      '#type' => 'pager'
    ];
    return $build;
    //return ['#theme' => 'datamanager_skill_list'];
  }

  public function datamanagerVirtues()
  {
    // Prepare _sortable_ table header
    $header = [
      ['data' => t('ID'), 'field' => 'vid'],
      ['data' => t('Name'), 'field' => 'title'],
      ['data' => t('Type'), 'field' => 'type'],
      ['data' => t('Options'), 'field' => 'options'],
      ['data' => t('Multiple'), 'field' => 'multiple'],
    ];

    $query = $this->database->select('mysticvirtues', 'miv');
    $query->fields('miv', ['vid', 'title', 'type', 'options', 'multiple']);
    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header);
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
    $result = $pager->execute();

    foreach ($result as $row) {
      $row = (array)$row;
      $row['multiple'] = ($row['multiple'] == 1 ? 'Yes' : 'No');
      $options = json_decode($row['options'], TRUE);
      $row['options'] = [];
      foreach($options as $option) {
        $row['options'][] = vsprintf("%s - %s", [$option['option'], $option['value']]);
      }
      $row['options'] = implode(', ', $row['options']);
      $row['title'] =  Link::createFromRoute($row['title'], 'datamanager.admin.virtues.edit', [ 'vid' => $row['vid'] ]);
      $rows[] = ['data' => $row];
    }

    $build['location_table'] = array(
      '#theme' => 'table', '#header' => $header,
      '#rows' => $rows
    );
    $build['pager'] = [
      '#type' => 'pager'
    ];
    return $build;
  }

  public function datamanagerAptitudes()
  {
    $search = $this->tempstore->get('aptitude_filters',[]);
    // Prepare _sortable_ table header
    $header = [
      ['data' => t('ID'), 'field' => 'aid'],
      ['data' => t('Name'), 'field' => 'title'],
      ['data' => t('Type'), 'field' => 'type'],
      ['data' => t('Aptitude Type'), 'field' => 'aptitudetype'],
      ['data' => t('Bloodline'), 'field' => 'bloodline'],
      ['data' => t('Tier'), 'field' => 'tier'],
    ];

    $query = $this->database->select('mysticaptitudes', 'map');
    $query->fields('map', ['aid', 'title', 'type', 'aptitudetype', 'bloodline','tier']);
      $inarray = []; $conditions = ['bloodline' => false, 'other' => false]; $list = [];

      foreach ($search as $key => $ping) {
        if ($ping === 1 && $key != 'bloodline') {
          $inarray[] = $key;
          $conditions['other'] = true;
          $list[] = $key;
        } else if ($ping === 1 && $key == 'bloodline') {
          $conditions['bloodline'] = true;
          $list[] = $key;
        }
      }

      if($conditions['bloodline'] && $conditions['other']) {
        $query->condition('bloodline', 0, '>');
        $query->condition('type', $inarray, 'IN');
      } else if ($conditions['bloodline'] && !$conditions['other']) {
        $query->condition('bloodline', '0', '>');
        if(!empty($inarray)) {
          $query->condition('type', $inarray, 'NOT IN');
        }
      } else if (!$conditions['bloodline'] && $conditions['other']) {
        $query->condition('type', $inarray, 'IN');
        $query->condition('bloodline', '0');
      } else if (!$conditions['bloodline'] && !$conditions['other']) {
        $query->condition('type', 'test');
      }

    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header);
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
    $result = $pager->execute();

    foreach ($result as $row) {
      $row = (array)$row;
      if($row['bloodline'] !== 0) {
        $bl = $this->dataservice->getMysticBloodlines($row['bloodline'])[0];
        $row['bloodline'] = Link::createFromRoute($bl['title'], 'datamanager.admin.bloodlines.edit', [ 'bid' => $bl['bid'] ]);;
      } else {
        $row['bloodline'] = 'None';
      }
      $row['title'] =  Link::createFromRoute($row['title'], 'datamanager.admin.aptitudes.edit', [ 'aid' => $row['aid'] ]);
      $rows[] = ['data' => $row];
    }

    $build['filter'] = [
      '#theme' => 'datamanager_aptitude_filter',
      '#selections' => $search,
      '#list' => implode(',', $list),
      '#attached' => [
        'library' => ['datamanager/datamanager-primary'],
      ],
    ];

    $build['location_table'] = [
      '#theme' => 'table', '#header' => $header,
      '#rows' => $rows
    ];
    $build['pager'] = [
      '#type' => 'pager'
    ];
    return $build;
  }

  public function datamanagerBloodlines()
  {
    // Prepare _sortable_ table header
    $header = [
      ['data' => t('ID'), 'field' => 'bid'],
      ['data' => t('Name'), 'field' => 'title'],
      ['data' => t('Type'), 'field' => 'type'],
    ];

    $query = $this->database->select('mysticbloodline', 'blo');
    $query->fields('blo', ['bid', 'title', 'type']);
    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header);
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
    $result = $pager->execute();

    foreach ($result as $row) {
      $row = (array)$row;
      $row['title'] =  Link::createFromRoute($row['title'], 'datamanager.admin.bloodlines.edit', [ 'bid' => $row['bid'] ]);
      $rows[] = ['data' => $row];
    }


    $build = [
      '#markup' => t('List of All locations')
    ];

    $build['location_table'] = array(
      '#theme' => 'table', '#header' => $header,
      '#rows' => $rows
    );
    $build['pager'] = [
      '#type' => 'pager'
    ];
    return $build;
  }

  public function datamanagerLanguage()
  {
    // Prepare _sortable_ table header
    $header = [
      ['data' => t('ID'), 'field' => 'lid'],
      ['data' => t('Name'), 'field' => 'title'],
      ['data' => t('Campaign'), 'field' => 'campaign'],
    ];

    $query = $this->database->select('mysticlanguage', 'mlk');
    $query->fields('mlk', ['lid', 'title', 'campaign']);
    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header);
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
    $result = $pager->execute();

    foreach ($result as $row) {
      $row = (array)$row;
      $row['title'] =  Link::createFromRoute($row['title'], 'datamanager.admin.language.edit', [ 'lid' => $row['lid'] ]);
      $rows[] = ['data' => $row];
    }


    $build = [
      '#markup' => t('List of All locations')
    ];

    $build['location_table'] = array(
      '#theme' => 'table', '#header' => $header,
      '#rows' => $rows
    );
    $build['pager'] = [
      '#type' => 'pager'
    ];
    return $build;
  }

  public function datamanagerCampaignsetting() {
    // Prepare _sortable_ table header
    $header = [
      ['data' => t('ID'), 'field' => 'csid'],
      ['data' => t('Name'), 'field' => 'title'],
      ['data' => t('Owner'), 'field' => 'owner'],
      ['data' => t('Public'), 'field' => 'public'],
      ['data' => t('Active'), 'field' => 'active'],
    ];

    $query = $this->database->select('mysticcampaignsetting', 'mcs');
    $query->fields('mcs', ['csid', 'title', 'owner', 'public', 'active']);
    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header);
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
    $result = $pager->execute();

    foreach ($result as $row) {
      $row = (array)$row;
      $account = \Drupal\user\Entity\User::load($row['owner']); // pass your uid
      $name = $account->getUsername();
      $row['public'] = ($row['public'] == 1 ? 'Yes' : 'No');
      $row['active'] = ($row['active'] == 1 ? 'Yes' : 'No');
      $row['owner'] =  (empty($name) ? 'Unowned':$name);
      $row['title'] =  Link::createFromRoute($row['title'], 'datamanager.admin.campaignsetting.edit', [ 'csid' => $row['csid'] ]);
      $rows[] = ['data' => $row];
    }

    $build = [
      '#markup' => t('List of All locations')
    ];

    $build['location_table'] = array(
      '#theme' => 'table', '#header' => $header,
      '#rows' => $rows
    );
    $build['pager'] = [
      '#type' => 'pager'
    ];
    return $build;
  }

  public function datamanagerRecordfilter() {

    $return = [
      'bloodline' => 0,
      'power' => 0,
      'technique' => 0,
      'talent' => 0,
    ];
    $elements = \Drupal::request()->request->get('elements');
    foreach($return as $key=>$el) {
      if(in_array($key, $elements)) {
        $return[$key] = 1;
      }
    };

    $this->tempstore->set('aptitude_filters', $return);

    return new JsonResponse(TRUE);
  }
}
